import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Calculator {
	
	
	
	public static void main (String []args) throws IOException
	{
		
		
		File file = new File("src/input.infix");
		FileReader fileReader=null;
		try {
			 fileReader = new FileReader(file);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		BufferedReader bufferedReader = new BufferedReader(fileReader);
		String line;
		while ((line = bufferedReader.readLine()) != null) {
			
			Expression nouveau=new Expression(line);
			
			nouveau.evaluate();
			
			
		}
	}

}
