import java.util.Stack;

public class Expression {
	private Stack<Token> list = new Stack<Token>();
	private Stack<Token> posfix = new Stack<Token>();

	public Expression(String t) {
		String[] aux = t.split("\\s+");
		for (String u : aux) {
			Token entry = null;

			switch (u) {
			case "+":
				entry = new Operator(opType.ADD);
				break;
			case "-":
				entry = new Operator(opType.SUB);
				break;
			case "*":
				entry = new Operator(opType.MULT);
				break;
			case "/":
				entry = new Operator(opType.DIV);
				break;
			case "%":
				entry = new Operator(opType.MOD);
				break;
			case "(":
				entry = new Operator(opType.LPAR);
				break;
			case ")":
				entry = new Operator(opType.RPAR);
				break;
			default:
				entry = new Operand(Integer.parseInt(u));
			}
			this.add_token(entry);

		}

	}

	public void inf2pos() {
		Operator end = new Operator(opType.RPAR);
		list.add(end);
		Operator begin = new Operator(opType.LPAR);
		Stack<Operator> aux = new Stack<Operator>();
		aux.push(begin);
		for (Token t : this.list) {
			if (t.isOperator()) {
				Operator t_aux = (Operator) t;
				switch (t.get_value()) {
				case "LParen":
					aux.push(t_aux);
					break;
				case "RParen":
					Token buffer = aux.pop();
					
					while (! buffer.get_value().equals( "LParen")) {
						posfix.add(buffer);
						
						buffer = aux.pop();
					}
					
					break;
				default:

					while (aux.size() > 0 && aux.peek().getPrec() >= t_aux.getPrec()) {
					posfix.add(aux.pop());
				

					}
					aux.push(t_aux);

					break;
				

				}
			} else {

				posfix.add(t);
			}

		}
		while(aux.size()>0)
			this.posfix.add(aux.pop());

	}

	public Token get_first() {
		return list.pop();
	}

	private void add_token(Token e) {
		if (e != null) {
			this.list.add(e);
		}
	}

	public Token put_first(Token t) {
		return list.push(t);
	}

	@Override
	public String toString() {
		StringBuilder aux = new StringBuilder("");
		for (Token t : this.list) {
			aux.append(t.toString());

		}
		return aux.toString();
	}
	public void evaluate()
	{
		this.inf2pos();
		System.out.print(this.print_pos());
		System.out.print("=");
		Stack<Integer> ops=new Stack<Integer>();
		for(Token t:this.posfix)
		{
			if(t.isOperand())
			{
				ops.push(new Integer(((Operand)t).getVal()));
				
			}
			else
			{
				int nr2=ops.pop();
				int nr1=ops.pop();
				int result=do_math((Operator)t,nr1,nr2);
				ops.push(new Integer(result));
			}
		}
		System.out.println(ops.pop().toString());
		
	}
	public int do_math(Operator op,int nr1,int nr2)
	{
		switch (op.get_value()) {
		case "Add":
			
			return nr1+nr2;
		case "Sub":
			
			return nr1-nr2;
		case "Mult":
			
			return nr1*nr2;
		case "Div":
			
			return nr1/nr2;
		case "Mod":
			
			return nr1%nr2;
		
			default:
				return -1;
			
		}
		
	}
	public String print_pos() {
		StringBuilder aux = new StringBuilder("");
		for (Token t : this.posfix) {
			aux.append(t.toString());
			aux.append(" ");

		}
		return aux.toString();
	}

}
